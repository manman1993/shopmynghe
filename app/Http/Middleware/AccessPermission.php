<?php

namespace App\Http\Middleware;

use Auth;
use App\Admin;
use Closure;
use Illuminate\Support\Facades\Route;

class AccessPermission
{
    /** No use model
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->hasAnyRoles(['admin', 'author'])) {
            return $next($request);
        }
        return redirect(('/dashboard'));
    }
}
