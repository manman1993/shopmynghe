<?php

use Illuminate\Database\Seeder;
use App\Admin;
use App\Roles;
use DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::truncate();
        DB::table('tbl_roles')->truncate();
        $adminRoles = Roles::where('name', 'admin')->first();
        $authorRoles = Roles::where('name', 'author')->first();
        $userRoles = Roles::where('name', 'user')->first();

        $admin = Admin::create([
            'admin_name' => 'manadmin',
            'admin_email' => 'manadmin@gmail.com',
            'admin_phone' => '123456789',
            'admin_password' => md5('123456')
        ]);
        $author = Admin::create([
            'admin_name' => 'manauthor',
            'admin_email' => 'manauthor@gmail.com',
            'admin_phone' => '123456789',
            'admin_password' => md5('123456')
        ]);
        $user = Admin::create([
            'admin_name' => 'manuser',
            'admin_email' => 'manuser@gmail.com',
            'admin_phone' => '123456789',
            'admin_password' => md5('123456')
        ]);

        $admin->roles()->attach($adminRoles);
        $author->roles()->attach($authorRoles);
        $user->roles()->attach($userRoles);
        factory(App\Admin::class, 20)->create();
    }
}
