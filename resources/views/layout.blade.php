<!doctype html>
<html lang="en">
   <meta http-equiv="content-type" content="text/html;charset=utf-8" />
   <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>Art Demo Store</title>
      <base href="{{('/frontend/')}}">
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link href="css/font-awesome.min.css" rel="stylesheet">
      <link href="css/prettyPhoto.css" rel="stylesheet">
      <link href="css/price-range.css" rel="stylesheet">
      <link href="css/animate.css" rel="stylesheet">
      <link href="css/main.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link href="css/sweetalert.css" rel="stylesheet">
      <link href="css/lightslider.css" rel="stylesheet">
      <link href="css/prettify.css" rel="stylesheet">
      <link href="css/lightgallery.min.css" rel="stylesheet">
      <meta name="description" content="Shop powered by PrestaShop">
      <meta name="keywords" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/vnd.microsoft.icon" href="img/favicond238.ico?1519024403">
      <link rel="shortcut icon" type="image/x-icon" href="img/favicond238.ico?1519024403">
      <!-- Templatemela added -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link href="https://fonts.googleapis.com/css?family=Sintony:400,700" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
      <link rel="stylesheet" href="themes/PRS050093/assets/css/theme.css" type="text/css" media="all">
      <link rel="stylesheet" href="modules/psblog/views/css/psblog.css" type="text/css" media="all">
      <link rel="stylesheet" href="js/jquery/ui/themes/base/minified/jquery-ui.min.css" type="text/css" media="all">
      <link rel="stylesheet" href="js/jquery/ui/themes/base/minified/jquery.ui.theme.min.css" type="text/css" media="all">
      <link rel="stylesheet" href="modules/ct_imageslider/views/css/flexslider.css" type="text/css" media="all">
      <link rel="stylesheet" href="themes/PRS050093/assets/css/custom.css" type="text/css" media="all">
      <script type="text/javascript">
         var prestashop = {"cart":{"products":[],"totals":{"total":{"type":"total","label":"Total","amount":0,"value":"$0.00"},"total_including_tax":{"type":"total","label":"Total (tax incl.)","amount":0,"value":"$0.00"},"total_excluding_tax":{"type":"total","label":"Total (tax excl.)","amount":0,"value":"$0.00"}},"subtotals":{"products":{"type":"products","label":"Subtotal","amount":0,"value":"$0.00"},"discounts":null,"shipping":{"type":"shipping","label":"Shipping","amount":0,"value":"Free"},"tax":{"type":"tax","label":"Taxes","amount":0,"value":"$0.00"}},"products_count":0,"summary_string":"0 items","labels":{"tax_short":"(tax excl.)","tax_long":"(tax excluded)"},"id_address_delivery":0,"id_address_invoice":0,"is_virtual":false,"vouchers":{"allowed":0,"added":[]},"discounts":[],"minimalPurchase":0,"minimalPurchaseRequired":""},"currency":{"name":"US Dollar","iso_code":"USD","iso_code_num":"840","sign":"$"},"customer":{"lastname":null,"firstname":null,"email":null,"last_passwd_gen":null,"birthday":null,"newsletter":null,"newsletter_date_add":null,"ip_registration_newsletter":null,"optin":null,"website":null,"company":null,"siret":null,"ape":null,"outstanding_allow_amount":0,"max_payment_days":0,"note":null,"is_guest":0,"id_shop":null,"id_shop_group":null,"id_default_group":1,"date_add":null,"date_upd":null,"reset_password_token":null,"reset_password_validity":null,"id":null,"is_logged":false,"gender":{"type":null,"name":null,"id":null},"risk":{"name":null,"color":null,"percent":null,"id":null},"addresses":[]},"language":{"name":"English (English)","iso_code":"en","locale":"en-US","language_code":"en-us","is_rtl":"0","date_format_lite":"m\/d\/Y","date_format_full":"m\/d\/Y H:i:s","id":1},"page":{"title":"","canonical":null,"meta":{"title":"Art Demo Store","description":"Shop powered by PrestaShop","keywords":"","robots":"index"},"page_name":"index","body_classes":{"lang-en":true,"lang-rtl":false,"country-US":true,"currency-USD":true,"layout-full-width":true,"page-index":true,"tax-display-disabled":true},"admin_notifications":[]},"shop":{"name":"Art Demo Store","email":"urcompany@gmail.com","registration_number":"","long":false,"lat":false,"logo":"\/prestashop\/PRS05\/PRS050093\/img\/art-demo-store-logo-1519024403.jpg","stores_icon":"\/prestashop\/PRS05\/PRS050093\/img\/logo_stores.png","favicon":"\/prestashop\/PRS05\/PRS050093\/img\/favicon.ico","favicon_update_time":"1519024403","address":{"formatted":"Art Demo Store<br>California<br>United States","address1":"","address2":"","postcode":"","city":"","state":"California","country":"United States"},"phone":"0123456789","fax":"465463"},"urls":{"base_url":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/","current_url":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php","shop_domain_url":"https:\/\/capricathemes.com","img_ps_url":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/","img_cat_url":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/c\/","img_lang_url":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/l\/","img_prod_url":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/","img_manu_url":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/m\/","img_sup_url":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/su\/","img_ship_url":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/s\/","img_store_url":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/st\/","img_col_url":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/co\/","img_url":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/themes\/PRS050093\/assets\/img\/","css_url":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/themes\/PRS050093\/assets\/css\/","js_url":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/themes\/PRS050093\/assets\/js\/","pic_url":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/upload\/","pages":{"address":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=address","addresses":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=addresses","authentication":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=authentication","cart":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=cart","category":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=category","cms":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=cms","contact":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=contact","discount":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=discount","guest_tracking":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=guest-tracking","history":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=history","identity":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=identity","index":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php","my_account":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=my-account","order_confirmation":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=order-confirmation","order_detail":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=order-detail","order_follow":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=order-follow","order":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=order","order_return":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=order-return","order_slip":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=order-slip","pagenotfound":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=pagenotfound","password":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=password","pdf_invoice":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=pdf-invoice","pdf_order_return":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=pdf-order-return","pdf_order_slip":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=pdf-order-slip","prices_drop":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=prices-drop","product":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=product","search":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=search","sitemap":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=sitemap","stores":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=stores","supplier":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=supplier","register":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=authentication&create_account=1","order_login":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=order&login=1"},"theme_assets":"\/prestashop\/PRS05\/PRS050093\/themes\/PRS050093\/assets\/","actions":{"logout":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?mylogout="}},"configuration":{"display_taxes_label":false,"low_quantity_threshold":3,"is_b2b":false,"is_catalog":false,"show_prices":true,"opt_in":{"partner":true},"quantity_discount":{"type":"discount","label":"Discount"},"voucher_enabled":0,"return_enabled":0,"number_of_days_for_return":14},"field_required":[],"breadcrumb":{"links":[{"title":"Home","url":"https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php"}],"count":1},"link":{"protocol_link":"https:\/\/","protocol_content":"https:\/\/"},"time":1603875622,"static_token":"d0079bf62de7300595f3731cc8e67631","token":"858056daa07e99773122634f8598b2de"};
      </script>
   </head>
   <body id="index" class="lang-en country-us currency-usd layout-full-width page-index tax-display-disabled">
      <div class="loadingdiv spinner"></div>
      <main>
         <header id="header">
            <nav class="header-nav">
               <div class="container">
                  <div class="row">
                     <div class="hidden-lg-up text-xs-left mobile">
                        <div class="pull-xs-left hidden-lg-up" id="menu-icon">
                           <i class="material-icons menu-open">&#xE5D2;</i>
                           <i class="material-icons menu-close">&#xE5CD;</i>
                        </div>
                        <div id="mobile_top_menu_wrapper" class="row hidden-lg-up" style="display:none;">
                           <div class="js-top-menu mobile" id="_mobile_top_menu"></div>
                           <div class="js-top-menu-bottom">
                              <div id="_mobile_currency_selector"></div>
                              <div id="_mobile_language_selector"></div>
                              <div id="_mobile_contact_link"></div>
                              <div id="_mobile_user_info"></div>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                     </div>

                     <div class="left-nav">
                        <div class=" pull-right">
                            <ul class="nav navbar-nav">

                            <li><a href="{{'/'}}"><i class="fa fa-star "></i> Trang chủ</a></li>
                                <?php
                                   $customer_id = Session::get('customer_id');
                                   $shipping_id = Session::get('shipping_id');
                                   if($customer_id!=NULL && $shipping_id==NULL){
                                 ?>
                                  <li><a href="{{URL::to('/checkout')}}"><i class="fa fa-crosshairs"></i> Thanh toán</a></li>

                                <?php
                                 }elseif($customer_id!=NULL && $shipping_id!=NULL){
                                 ?>
                                 <li><a href="{{URL::to('/payment')}}"><i class="fa fa-crosshairs"></i> Thanh toán</a></li>
                                 <?php
                                }else{
                                ?>
                                 <li><a href="{{URL::to('/dang-nhap')}}"><i class="fa fa-crosshairs"></i> Thanh toán</a></li>
                                <?php
                                 }
                                ?>


                                <li><a href="{{URL::to('/gio-hang')}}"><i class="fa fa-shopping-cart"></i> Giỏ hàng</a></li>
                                <?php
                                   $customer_id = Session::get('customer_id');
                                   if($customer_id!=NULL){
                                 ?>
                                  <li><a href="{{URL::to('/logout-checkout')}}"><i class="fa fa-lock"></i> Đăng xuất</a></li>

                                <?php
                            }else{
                                 ?>
                                 <li><a href="{{URL::to('/dang-nhap')}}"><i class="fa fa-lock"></i> Đăng nhập</a></li>
                                 <?php
                             }
                                 ?>

                            </ul>
                        </div>
                    </div>
                     <div class="right-nav">
                        <!-- Block search module TOP -->
                        <div id="search_widget" class="col-lg-4 col-md-5 col-sm-12 search-widget" data-search-controller-url="indexbd1a.html?controller=search">
                           <span class="search_button"><i class="material-icons search">&#xE8B6;</i><i class="material-icons cross">&#xE14C;</i><span>Search</span></span>
                           <div >
                            <form action="{{URL::to('/tim-kiem')}}" method="POST">
                                {{csrf_field()}}
                                <input type="text" name="keywords_submit" placeholder="Tìm kiếm sản phẩm"/>
                                {{-- <input type="submit" style="margin-top:0;color:#666" name="search_items" class="btn btn-primary btn-sm" value="Tìm kiếm"> --}}
                                 <button  type="submit">
                                 <i class="material-icons search">&#xE8B6;</i>
                                 </button>
                              </form>
                           </div>
                        </div>
                        <!-- /Block search module TOP -->
                        <div id="desktop_cart">
                           <div class="blockcart cart-preview inactive" data-refresh-url="index22d1.json?fc=module&amp;module=ps_shoppingcart&amp;controller=ajax&amp;id_lang=1">
                              <div class="header blockcart-header dropdown js-dropdown">
                                 <a rel="nofollow" href="indexb379.html?controller=cart&amp;action=show"  >
                                    <i class="material-icons shopping-cart"></i>
                                    <span class="hidden-md-down title">Cart</span>
                                    <span class="cart-products-count">0</span>
                                    <!--<span class="hidden-md-down item">Item(s) - </span>
                                       <span class="hidden-md-down value">$0.00</span>-->
                                 </a>
                              </div>
                           </div>
                        </div>
                        <i class="material-icons more-icon">&#xE5D4;</i>
                     </div>
                  </div>
               </div>
            </nav>
            <div class="header-top hidden-md-down">
               <div class="container">
                  <div class="row">
                     <div class="top-logo" id="_mobile_logo"></div>
                     <div class="main-logo">
                        <div class="header_logo" id="_desktop_logo">
                           <a href="index-2.html">
                           <img class="logo img-responsive" src="img/art-demo-store-logo-1519024403.jpg" alt="Art Demo Store">
                           </a>
                        </div>
                     </div>
                     <div id="ctcmsblock">
                        <div class="cmsblock slide">
                           <ul>
                              <li><a href="#"><i class="material-icons phone"></i>contact</a></li>
                              <li><a href="#"><i class="material-icons sitemap"></i>sitemap</a></li>
                              <li><a href="#"><i class="material-icons bookmark"></i>bookmark</a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="pull-xs-right" id="_mobile_cart"></div>
                  </div>
               </div>
            </div>
            <div class="container">
               <div class="row">
                  <div class="menu col-lg-8 col-md-7 js-top-menu position-static hidden-sm-down" id="_desktop_top_menu">
                     <ul class="top-menu" id="top-menu" data-depth="0">
                        @foreach($category as $key => $cate)
                            <li class="category" id="category-3">
                            <a  class="dropdown-item"
                                href="{{URL::to('/danh-muc/'.$cate->slug_category_product)}}" data-depth="0">
                                {{$cate->category_name}}
                            </a>
                            </li>

                        @endforeach
                        @foreach($brand as $key => $brand)
                        <li class="category" id="category-3">
                            <a  class="dropdown-item"
                                href="{{URL::to('/thuong-hieu/'.$brand->brand_slug)}}" data-depth="0">
                                {{$brand->brand_name}}
                            </a>
                            </li>
                        @endforeach
                        <li class="cms-page" id="cms-page-4">
                           <a
                              class="dropdown-item"
                              href="indexac29.html?id_cms=4&amp;controller=cms&amp;id_lang=1" data-depth="0"
                              >
                           About us
                           </a>
                        </li>
                     </ul>
                     <div class="clearfix"></div>
                  </div>
               </div>
            </div>
            <nav data-depth="1" class="breadcrumb hidden-sm-down">
               <div class="container">
                  <ol itemscope itemtype="http://schema.org/BreadcrumbList">
                     <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a itemprop="item" href="index.html">
                        <span itemprop="name">Home</span>
                        </a>
                        <meta itemprop="position" content="1">
                     </li>
                  </ol>
               </div>
            </nav>
         </header>
         <aside id="notifications">
            <div class="container">
            </div>
         </aside>
         <section id="wrapper">
            <div class="home_container">
               <div id="columns_inner">
                  <div id="content-wrapper">
                     <section id="main">
                        <section id="content" class="page-home">
                           <div class="flexslider" data-interval="3000" data-pause="true">

                              <ul class="slides">
                                @php
                                $i = 0;
                                @endphp
                                @foreach($slider as $key => $slide)
                                 <li class="slide">
                                    @php
                                    $i++;
                                @endphp
                                    <img alt="{{$slide->slider_desc}}" src="{{asset('public/uploads/slider/'.$slide->slider_image)}}" height="200" width="100%" class="img img-responsive img-slider">
                                </li>
                                 @endforeach
                              </ul>
                           </div>
                           <div id="ctservicecmsblock">
                              <div class="container">
                                 <div class="row">
                                    <div class="serviceblock hb-animate-element right-to-left">
                                       <div class="service-1 service col-sm-4">
                                          <span class="service1-icon icon"></span>
                                          <div class="service-content">
                                             <div class="content-title">Free Shipping</div>
                                             <div class="content-desc">Curabitur blandit tempus ardua ridiculus doloresed magna.</div>
                                          </div>
                                       </div>
                                       <div class="service2 service col-sm-4">
                                          <span class="service2-icon icon"></span>
                                          <div class="service-content">
                                             <div class="content-title">online support</div>
                                             <div class="content-desc">Curabitur blandit tempus ardua ridiculus doloresed magna.</div>
                                          </div>
                                       </div>
                                       <div class="service-3 service col-sm-4">
                                          <span class="service3-icon icon"></span>
                                          <div class="service-content">
                                             <div class="content-title">special offer</div>
                                             <div class="content-desc">Curabitur blandit tempus ardua ridiculus doloresed magna.</div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <section class="special-products">
                              <div class="container">
                                @yield('content')
                              </div>
                           </section>

         <footer id="footer">
            <div class="footer-before">
               <div class="container">
                  <div class="row">
                     <div class="block_newsletter col-lg-12 col-md-12 col-sm-12 hb-animate-element top-to-bottom">
                        <p class="col-xs-12 title">Join the community to be updated firstly?
                           <span class="subtitle">You may unsubscribe at any moment. For that purpose, please find our contact info in the legal notice.</span>
                        </p>
                        <div class="col-md-7 col-xs-12 form">
                           <form action="https://capricathemes.com/prestashop/PRS05/PRS050093/index.php#footer" method="post">
                              <div class="row">
                                 <div class="col-xs-12 inputbox">
                                    <input
                                       class="btn btn-primary pull-xs-right hidden-xs-down"
                                       name="submitNewsletter"
                                       type="submit"
                                       value="Subscribe"
                                       >
                                    <input
                                       class="btn btn-primary pull-xs-right hidden-sm-up"
                                       name="submitNewsletter"
                                       type="submit"
                                       value="OK"
                                       >
                                    <div class="input-wrapper">
                                       <input
                                          name="email"
                                          type="text"
                                          value=""
                                          placeholder="Enter your email address....."
                                          >
                                    </div>
                                    <input type="hidden" name="action" value="0">
                                    <div class="clearfix"></div>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="footer-container hb-animate-element right-to-left">
               <div class="container">
                  <div class="row">
                     <div class="footer">
                        <div id="ctfootercmsblock" class="block">
                           <div class="block_about">
                              <div class="toggle-footer">
                                 <div class="footer_logo"><a href="#"><img src="img/cms/Footer-Logo.png" alt="footer logo" /></a></div>
                                 <div class="footer-about-desc">
                                    <p>Sed quia consequuntur magni dolor qui ratione porro tatem sequineque porro.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="block-contact footer-block col-xs-12 col-sm-4 links wrapper">
                           <h3 class="text-uppercase block-contact-title hidden-md-down"><a href="index11f6.html?controller=stores">Contact Us</a></h3>
                           <div class="title clearfix hidden-lg-up" data-target="#block-contact_list" data-toggle="collapse">
                              <span class="h3">Store information</span>
                              <span class="pull-xs-right">
                              <span class="navbar-toggler collapse-icons">
                              <i class="material-icons add">&#xE313;</i>
                              <i class="material-icons remove">&#xE316;</i>
                              </span>
                              </span>
                           </div>
                           <ul id="block-contact_list" class="collapse">
                              <li>
                                 <i class="material-icons address">&#xE55F;</i>
                                 <span class="contactdiv"> Art Demo Store<br />California<br />United States</span>
                              </li>
                              <li>
                                 <i class="material-icons phone">&#xE324;</i>
                                 <span>0123456789</span>
                              </li>
                              <li>
                                 <i id="fax" class="material-icons"></i>
                                 <span>465463</span>
                              </li>
                              <li>
                                 <i class="material-icons mail">&#xE554;</i>
                                 <span>urcompany@gmail.com</span>
                              </li>
                           </ul>
                        </div>
                        <div class="col-md-4 links block links">
                           <h3 class="h3 hidden-md-down"><i class="material-icons">&#xE3C7;</i>Products</h3>
                           <div class="title h3 block_title hidden-lg-up" data-target="#footer_sub_menu_58064" data-toggle="collapse">
                              <span class="">Products</span>
                              <span class="pull-xs-right">
                              <span class="navbar-toggler collapse-icons">
                              <i class="material-icons add">&#xE313;</i>
                              <i class="material-icons remove">&#xE316;</i>
                              </span>
                              </span>
                           </div>
                           <ul id="footer_sub_menu_58064" class="collapse block_content">
                              <li>
                                 <a
                                    id="link-cms-page-1-1"
                                    class="cms-page-link"
                                    href="indexab5b.html?id_cms=1&amp;controller=cms&amp;id_lang=1"
                                    title="Our terms and conditions of delivery">
                                 Delivery
                                 </a>
                              </li>
                              <li>
                                 <a
                                    id="link-cms-page-5-1"
                                    class="cms-page-link"
                                    href="indexac7f.html?id_cms=5&amp;controller=cms&amp;id_lang=1"
                                    title="Our secure payment method">
                                 Secure payment
                                 </a>
                              </li>
                              <li>
                                 <a
                                    id="link-product-page-prices-drop-1"
                                    class="cms-page-link"
                                    href="indexc485.html?controller=prices-drop"
                                    title="On-sale products">
                                 Prices drop
                                 </a>
                              </li>
                              <li>
                                 <a
                                    id="link-product-page-new-products-1"
                                    class="cms-page-link"
                                    href="index6237.html?controller=new-products"
                                    title="Our new products">
                                 New products
                                 </a>
                              </li>
                              <li>
                                 <a
                                    id="link-product-page-best-sales-1"
                                    class="cms-page-link"
                                    href="index7e4b.html?controller=best-sales"
                                    title="Our best sales">
                                 Best sales
                                 </a>
                              </li>
                              <li>
                                 <a
                                    id="link-static-page-stores-1"
                                    class="cms-page-link"
                                    href="index11f6.html?controller=stores"
                                    title="">
                                 Stores
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="col-md-4 links block links">
                           <h3 class="h3 hidden-md-down"><i class="material-icons">&#xE3C7;</i>Our company</h3>
                           <div class="title h3 block_title hidden-lg-up" data-target="#footer_sub_menu_4617" data-toggle="collapse">
                              <span class="">Our company</span>
                              <span class="pull-xs-right">
                              <span class="navbar-toggler collapse-icons">
                              <i class="material-icons add">&#xE313;</i>
                              <i class="material-icons remove">&#xE316;</i>
                              </span>
                              </span>
                           </div>
                           <ul id="footer_sub_menu_4617" class="collapse block_content">
                              <li>
                                 <a
                                    id="link-cms-page-2-2"
                                    class="cms-page-link"
                                    href="index0913.html?id_cms=2&amp;controller=cms&amp;id_lang=1"
                                    title="Legal notice">
                                 Legal Notice
                                 </a>
                              </li>
                              <li>
                                 <a
                                    id="link-cms-page-3-2"
                                    class="cms-page-link"
                                    href="index7b3f.html?id_cms=3&amp;controller=cms&amp;id_lang=1"
                                    title="Our terms and conditions of use">
                                 Terms and conditions of use
                                 </a>
                              </li>
                              <li>
                                 <a
                                    id="link-cms-page-4-2"
                                    class="cms-page-link"
                                    href="indexac29.html?id_cms=4&amp;controller=cms&amp;id_lang=1"
                                    title="Learn more about us">
                                 About us
                                 </a>
                              </li>
                              <li>
                                 <a
                                    id="link-static-page-contact-2"
                                    class="cms-page-link"
                                    href="index0684.html?controller=contact"
                                    title="Use our form to contact us">
                                 Contact us
                                 </a>
                              </li>
                              <li>
                                 <a
                                    id="link-static-page-sitemap-2"
                                    class="cms-page-link"
                                    href="indexcc05.html?controller=sitemap"
                                    title="Lost ? Find what your are looking for">
                                 Sitemap
                                 </a>
                              </li>
                              <li>
                                 <a
                                    id="link-static-page-my-account-2"
                                    class="cms-page-link"
                                    href="index6714.html?controller=my-account"
                                    title="">
                                 My account
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="block-social">
                           <ul>
                              <li class="facebook"><a href="#" target="_blank">Facebook</a></li>
                              <li class="twitter"><a href="#" target="_blank">Twitter</a></li>
                              <li class="googleplus"><a href="#" target="_blank">Google +</a></li>
                              <li class="instagram"><a href="#" target="_blank">Instagram</a></li>
                           </ul>
                        </div>
                        <script type="text/javascript">
                           var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();


                           (function(){
                           var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                           s1.async=true;
                           s1.src='https://embed.tawk.to/5ab4ccaa4b401e45400dfc52/default';
                           s1.charset='UTF-8';
                           s1.setAttribute('crossorigin','*');
                           s0.parentNode.insertBefore(s1,s0);
                           })();
                        </script>
                        <!--End of tawk.to Script-->
                     </div>
                  </div>
               </div>
            </div>
            <div class="footer-bottom">
               <div class="container">
                  <div class="row">
                     <div class="footer-after col-md-12">
                        <p class="copyright hb-animate-element bottom-to-top">
                           <a class="_blank" href="http://www.prestashop.com/" target="_blank">
                           © 2020 - Ecommerce software by PrestaShop™
                           </a>
                        </p>
                        <div id="ctpaymentcmsblock">
                           <ul class="payment-cms hb-animate-element bottom-to-top">
                              <li class="visa"><a href="#"></a></li>
                              <li class="paypal"><a href="#"></a></li>
                              <li class="mastercard"><a href="#"></a></li>
                              <li class="discover"><a href="#"></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <a class="top_button" href="#" style=""><span><i class="material-icons">&#xE5C7;</i></span></a>
         </footer>
      </main>
      <script type="text/javascript" src="themes/core.js" ></script>
      <script type="text/javascript" src="themes/PRS050093/assets/js/theme.js" ></script>
      <script type="text/javascript" src="js/jquery/ui/jquery-ui.min.js" ></script>
      <script type="text/javascript" src="modules/ps_searchbar/ps_searchbar.js" ></script>
      <script type="text/javascript" src="modules/ps_shoppingcart/ps_shoppingcart.js" ></script>
      <script type="text/javascript" src="modules/ct_imageslider/views/js/jquery.flexslider-min.js" ></script>
      <script type="text/javascript" src="themes/PRS050093/assets/js/owl.carousel.js" ></script>
      <script type="text/javascript" src="themes/PRS050093/assets/js/totalstorage.js" ></script>
      <script type="text/javascript" src="themes/PRS050093/assets/js/waypoints.min.js" ></script>
      <script type="text/javascript" src="themes/PRS050093/assets/js/inview.js" ></script>
      <script type="text/javascript" src="themes/PRS050093/assets/js/lightbox-2.6.min.js" ></script>
      <script type="text/javascript" src="themes/PRS050093/assets/js/custom.js" ></script>



      {{-- <script src="js/jquery.js"></script> --}}
      <script src="js/bootstrap.min.js"></script>
      <script src="js/jquery.scrollUp.min.js"></script>
      <script src="js/price-range.js"></script>
      <script src="js/jquery.prettyPhoto.js"></script>
      {{-- <script src="js/main.js"></script> --}}
      <script src="js/sweetalert.min.js"></script>
      <script src="js/lightslider.js"></script>
      <script src="js/prettify.js"></script>
      <script src="js/lightgallery-all.min.js"></script>

     {{--  <script src="https://www.paypal.com/sdk/js?client-id=sb"></script>
      <script>paypal.Buttons().render('body');</script> --}}
      <script src="https://www.google.com/recaptcha/api.js" async defer></script>
      <div id="fb-root"></div>
  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v6.0&appId=2339123679735877&autoLogAppEvents=1"></script>

    <script type="text/javascript">
          $(document).ready(function() {
              $('#imageGallery').lightSlider({
                  gallery:true,
                  item:1,
                  loop:true,
                  thumbItem:3,
                  slideMargin:0,
                  enableDrag: false,
                  currentPagerPosition:'left',
                  onSliderLoad: function(el) {
                      el.lightGallery({
                          selector: '#imageGallery .lslide'
                      });
                  }
              });
          });
      </script>
      <script type="text/javascript">

            $(document).ready(function(){
              $('.send_order').click(function(){
                  swal({
                    title: "Xác nhận đơn hàng",
                    text: "Đơn hàng sẽ không được hoàn trả khi đặt,bạn có muốn đặt không?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Cảm ơn, Mua hàng",

                      cancelButtonText: "Đóng,chưa mua",
                      closeOnConfirm: false,
                      closeOnCancel: false
                  },
                  function(isConfirm){
                       if (isConfirm) {
                          var shipping_email = $('.shipping_email').val();
                          var shipping_name = $('.shipping_name').val();
                          var shipping_address = $('.shipping_address').val();
                          var shipping_phone = $('.shipping_phone').val();
                          var shipping_notes = $('.shipping_notes').val();
                          var shipping_method = $('.payment_select').val();
                          var order_fee = $('.order_fee').val();
                          var order_coupon = $('.order_coupon').val();
                          var _token = $('input[name="_token"]').val();

                          $.ajax({
                              url: '{{url('/confirm-order')}}',
                              method: 'POST',
                              data:{shipping_email:shipping_email,shipping_name:shipping_name,shipping_address:shipping_address,shipping_phone:shipping_phone,shipping_notes:shipping_notes,_token:_token,order_fee:order_fee,order_coupon:order_coupon,shipping_method:shipping_method},
                              success:function(){
                                 swal("Đơn hàng", "Đơn hàng của bạn đã được gửi thành công", "success");
                              }
                          });

                          window.setTimeout(function(){
                              location.reload();
                          } ,3000);

                        } else {
                          swal("Đóng", "Đơn hàng chưa được gửi, làm ơn hoàn tất đơn hàng", "error");

                        }

                  });


              });
          });


      </script>
      <script type="text/javascript">
  // sweetalert
          $(document).ready(function(){
              $('.add-to-cart').click(function(){
                  var id = $(this).data('id_product');
                  var cart_product_id = $('.cart_product_id_' + id).val();
                  var cart_product_name = $('.cart_product_name_' + id).val();
                  var cart_product_image = $('.cart_product_image_' + id).val();
                  var cart_product_quantity = $('.cart_product_quantity_' + id).val();
                  var cart_product_price = $('.cart_product_price_' + id).val();
                  var cart_product_sale_price = $('.cart_product_sale_price_' + id).val();
                  var cart_product_qty = $('.cart_product_qty_' + id).val();
                  var _token = $('input[name="_token"]').val();
                  if(parseInt(cart_product_qty)>parseInt(cart_product_quantity)){
                      alert('Làm ơn đặt nhỏ hơn ' + cart_product_quantity);
                  }else{
                      $.ajax({
                          url: '{{url('/add-cart-ajax')}}',
                          method: 'POST',
                          data:{cart_product_id:cart_product_id,cart_product_name:cart_product_name,cart_product_image:cart_product_image,cart_product_price:cart_product_price,cart_product_sale_price:cart_product_sale_price,cart_product_qty:cart_product_qty,_token:_token,cart_product_quantity:cart_product_quantity},
                          success:function(){

                              swal({
                                      title: "Đã thêm sản phẩm vào giỏ hàng",
                                      text: "Bạn có thể mua hàng tiếp hoặc tới giỏ hàng để tiến hành thanh toán",
                                      showCancelButton: true,
                                      cancelButtonText: "Xem tiếp",
                                      confirmButtonClass: "btn-success",
                                      confirmButtonText: "Đi đến giỏ hàng",
                                      closeOnConfirm: false
                                  },
                                  function() {
                                      window.location.href = "{{url('/gio-hang')}}";
                                  });
                          }
                      });
                  }
              });
          });
      </script>
      <script type="text/javascript">
          $(document).ready(function(){
              $('.choose').on('change',function(){
              var action = $(this).attr('id');
              var ma_id = $(this).val();
              var _token = $('input[name="_token"]').val();
              var result = '';

              if(action=='city'){
                  result = 'province';
              }else{
                  result = 'wards';
              }
              $.ajax({
                  url : '{{url('/select-delivery-home')}}',
                  method: 'POST',
                  data:{action:action,ma_id:ma_id,_token:_token},
                  success:function(data){
                     $('#'+result).html(data);
                  }
              });
          });
          });

      </script>
      <script type="text/javascript">
          $(document).ready(function(){
              $('.calculate_delivery').click(function(){
                  var matp = $('.city').val();
                  var maqh = $('.province').val();
                  var xaid = $('.wards').val();
                  var _token = $('input[name="_token"]').val();
                  if(matp == '' && maqh =='' && xaid ==''){
                      alert('Làm ơn chọn để tính phí vận chuyển');
                  }else{
                      $.ajax({
                      url : '{{url('/calculate-fee')}}',
                      method: 'POST',
                      data:{matp:matp,maqh:maqh,xaid:xaid,_token:_token},
                      success:function(){
                         location.reload();
                      }
                      });
                  }
          });
      });
      </script>
   </body>
</html>
