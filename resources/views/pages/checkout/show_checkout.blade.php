@extends('layout')
@section('content')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="{{URL::to('/')}}">Trang chủ</a></li>
              <li class="active">Thanh toán giỏ hàng</li>
            </ol>
        </div>

        <div class="register-req">
            <p>Làm ơn đăng ký hoặc đăng nhập để thanh toán giỏ hàng và xem lại lịch sử mua hàng</p>
        </div><!--/register-req-->

        <div class="shopper-informations">
            <div class="row">

                <div class="col-sm-12 clearfix">
                    <div class="bill-to">
                        <p>Điền thông tin gửi hàng</p>
                        <div class="form-one">
                            <form method="POST">
                                @csrf
                                <input type="text" name="shipping_email" class="shipping_email" placeholder="Điền email">
                                <input type="text" name="shipping_name" class="shipping_name" placeholder="Họ và tên người gửi">
                                <input type="text" name="shipping_address" class="shipping_address" placeholder="Địa chỉ gửi hàng">
                                <input type="text" name="shipping_phone" class="shipping_phone" placeholder="Số điện thoại">
                                <textarea name="shipping_notes" class="shipping_notes" placeholder="Ghi chú đơn hàng của bạn" rows="5"></textarea>

                                @if(Session::get('fee'))
                                    <input type="hidden" name="order_fee" class="order_fee" value="{{Session::get('fee')}}">
                                @else
                                    <input type="hidden" name="order_fee" class="order_fee" value="10000">
                                @endif

                                @if(Session::get('coupon'))
                                    @foreach(Session::get('coupon') as $key => $cou)
                                        <input type="hidden" name="order_coupon" class="order_coupon" value="{{$cou['coupon_code']}}">
                                    @endforeach
                                @else
                                    <input type="hidden" name="order_coupon" class="order_coupon" value="no">
                                @endif



                                <div class="">
                                     <div class="form-group">
                                        <label for="exampleInputPassword1">Chọn hình thức thanh toán</label>
                                          <select name="payment_select"  class="form-control input-sm m-bot15 payment_select">
                                                <option value="0">Qua chuyển khoản</option>
                                                <option value="1">Tiền mặt</option>
                                        </select>
                                    </div>
                                </div>
                                <input type="button" value="Xác nhận đơn hàng" name="send_order" class="btn btn-primary btn-sm send_order">
                            </form>
                            <form>
                                @csrf

                            <div class="form-group">
                                <label for="exampleInputPassword1">Chọn thành phố</label>
                                  <select name="city" id="city" class="form-control input-sm m-bot15 choose city">

                                        <option value="">--Chọn tỉnh thành phố--</option>
                                    @foreach($city as $key => $ci)
                                        <option value="{{$ci->matp}}">{{$ci->name_city}}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Chọn quận huyện</label>
                                  <select name="province" id="province" class="form-control input-sm m-bot15 province choose">
                                        <option value="">--Chọn quận huyện--</option>

                                </select>
                            </div>
                              <div class="form-group">
                                <label for="exampleInputPassword1">Chọn xã phường</label>
                                  <select name="wards" id="wards" class="form-control input-sm m-bot15 wards">
                                        <option value="">--Chọn xã phường--</option>
                                </select>
                            </div>


                              <input type="button" value="Tính phí vận chuyển" name="calculate_order" class="btn btn-primary btn-sm calculate_delivery">


                            </form>

                        </div>

                    </div>
                </div>
                <div class="col-sm-12 clearfix">
                      @if(session()->has('message'))
                            <div class="alert alert-success">
                                {!! session()->get('message') !!}
                            </div>
                        @elseif(session()->has('error'))
                             <div class="alert alert-danger">
                                {!! session()->get('error') !!}
                            </div>
                        @endif
                    <div class="table-responsive cart_info">

                        <form action="{{url('/update-cart')}}" method="POST">
                            @csrf
                        <table class="table table-condensed">
                            <thead>
                                <tr class="cart_menu">
                                    <td class="image">Hình ảnh</td>
                                    <td class="description">Tên sản phẩm</td>
                                    <td class="price">Giá sản phẩm</td>
                                    <td class="quantity">Số lượng</td>
                                    <td class="total">Thành tiền</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                @if(Session::get('cart')==true)
                                @php
                                        $total = 0;
                                @endphp
                                @foreach(Session::get('cart') as $key => $cart)
                                    @php
                                        $subtotal = $cart['product_price']*$cart['product_qty'];
                                        $total+=$subtotal;
                                    @endphp

                                <tr>
                                    <td class="cart_product">
                                        <img src="{{asset('public/uploads/product/'.$cart['product_image'])}}" width="90" alt="{{$cart['product_name']}}" />
                                    </td>
                                    <td class="cart_description">
                                        <h4><a href=""></a></h4>
                                        <p>{{$cart['product_name']}}</p>
                                    </td>
                                    <td class="cart_price">
                                        <p>{{number_format($cart['product_price'],0,',','.')}}đ</p>
                                    </td>
                                    <td class="cart_quantity">
                                        <div class="cart_quantity_button">


                                            <input class="cart_quantity" type="number" min="1" name="cart_qty[{{$cart['session_id']}}]" value="{{$cart['product_qty']}}"  >


                                        </div>
                                    </td>
                                    <td class="cart_total">
                                        <p class="cart_total_price">
                                            {{number_format($subtotal,0,',','.')}}đ

                                        </p>
                                    </td>
                                    <td class="cart_delete">
                                        <a class="cart_quantity_delete" href="{{url('/del-product/'.$cart['session_id'])}}"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>

                                @endforeach
                                <tr>
                                    <td><input type="submit" value="Cập nhật giỏ hàng" name="update_qty" class="check_out btn btn-default btn-sm"></td>
                                    <td><a class="btn btn-default check_out" href="{{url('/del-all-product')}}">Xóa tất cả</a></td>
                                    <td>
                                        @if(Session::get('coupon'))
                                          <a class="btn btn-default check_out" href="{{url('/unset-coupon')}}">Xóa mã khuyến mãi</a>
                                        @endif
                                    </td>


                                    <td colspan="2">
                                    <li>Tổng tiền :<span>{{number_format($total,0,',','.')}}đ</span></li>
                                    @if(Session::get('coupon'))
                                    <li>

                                            @foreach(Session::get('coupon') as $key => $cou)
                                                @if($cou['coupon_condition']==1)
                                                    Mã giảm : {{$cou['coupon_number']}} %
                                                    <p>
                                                        @php
                                                        $total_coupon = ($total*$cou['coupon_number'])/100;

                                                        @endphp
                                                    </p>
                                                    <p>
                                                    @php
                                                        $total_after_coupon = $total-$total_coupon;
                                                    @endphp
                                                    </p>
                                                @elseif($cou['coupon_condition']==2)
                                                    Mã giảm : {{number_format($cou['coupon_number'],0,',','.')}} k
                                                    <p>
                                                        @php
                                                        $total_coupon = $total - $cou['coupon_number'];

                                                        @endphp
                                                    </p>
                                                    @php
                                                        $total_after_coupon = $total_coupon;
                                                    @endphp
                                                @endif
                                            @endforeach



                                    </li>
                                    @endif

                                    @if(Session::get('fee'))
                                    <li>
                                        <a class="cart_quantity_delete" href="{{url('/del-fee')}}"><i class="fa fa-times"></i></a>

                                        Phí vận chuyển <span>{{number_format(Session::get('fee'),0,',','.')}}đ</span></li>
                                        <?php $total_after_fee = $total + Session::get('fee'); ?>
                                    @endif
                                    <li>Tổng còn:
                                    @php
                                        if(Session::get('fee') && !Session::get('coupon')){
                                            $total_after = $total_after_fee;
                                            echo number_format($total_after,0,',','.').'đ';
                                        }elseif(!Session::get('fee') && Session::get('coupon')){
                                            $total_after = $total_after_coupon;
                                            echo number_format($total_after,0,',','.').'đ';
                                        }elseif(Session::get('fee') && Session::get('coupon')){
                                            $total_after = $total_after_coupon;
                                            $total_after = $total_after + Session::get('fee');
                                            echo number_format($total_after,0,',','.').'đ';
                                        }elseif(!Session::get('fee') && !Session::get('coupon')){
                                            $total_after = $total;
                                            echo number_format($total_after,0,',','.').'đ';
                                        }

                                    @endphp
                                    </li>

                                </td>
                                </tr>
                                @else
                                <tr>
                                    <td colspan="5"><center>
                                    @php
                                    echo 'Làm ơn thêm sản phẩm vào giỏ hàng';
                                    @endphp
                                    </center></td>
                                </tr>
                                @endif
                            </tbody>



                        </form>
                            @if(Session::get('cart'))
                            <tr><td>

                                    <form method="POST" action="{{url('/check-coupon')}}">
                                        @csrf
                                            <input type="text" class="form-control" name="coupon" placeholder="Nhập mã giảm giá"><br>
                                              <input type="submit" class="btn btn-default check_coupon" name="check_coupon" value="Tính mã giảm giá">

                                          </form>
                                      </td>
                            </tr>
                            @endif

                        </table>

                    </div>
                </div>

            </div>
        </div>




    </div>
</section> <!--/#cart_items-->
{{--  --}}
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="{{URL::to('/')}}">Trang chủ</a></li>
              <li class="active">Giỏ hàng của bạn</li>
            </ol>
        </div>
        <div class="table-responsive cart_info">
            <?php
            $content = Cart::content();

            ?>
            <table class="table table-condensed">
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Hình ảnh</td>
                        <td class="description">Tên sp</td>
                        <td class="price">Giá</td>
                        <td class="quantity">Số lượng</td>
                        <td class="total">Tổng</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($content as $v_content)
                    <tr>
                        <td class="cart_product">
                            <a href=""><img src="{{URL::to('public/uploads/product/'.$v_content->options->image)}}" width="90" alt="" /></a>
                        </td>
                        <td class="cart_description">
                            <h4><a href="">{{$v_content->name}}</a></h4>
                            <p>Web ID: 1089772</p>
                        </td>
                        <td class="cart_price">
                            <p>{{number_format($v_content->price).' '.'vnđ'}}</p>
                        </td>
                        <td class="cart_quantity">
                            <div class="cart_quantity_button">
                                <form action="{{URL::to('/update-cart-quantity')}}" method="POST">
                                {{ csrf_field() }}
                                <input class="cart_quantity_input" type="text" name="cart_quantity" value="{{$v_content->qty}}"  >
                                <input type="hidden" value="{{$v_content->rowId}}" name="rowId_cart" class="form-control">
                                <input type="submit" value="Cập nhật" name="update_qty" class="btn btn-default btn-sm">
                                </form>
                            </div>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price">

                                <?php
                                $subtotal = $v_content->price * $v_content->qty;
                                echo number_format($subtotal).' '.'vnđ';
                                ?>
                            </p>
                        </td>
                        <td class="cart_delete">
                            <a class="cart_quantity_delete" href="{{URL::to('/delete-to-cart/'.$v_content->rowId)}}"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section> <!--/#cart_items-->

<section id="do_action">
    <div class="container">

        <div class="row">

            <div class="col-sm-6">
                <div class="total_area">
                    <ul>
                        <li>Tổng <span>{{Cart::total().' '.'vnđ'}}</span></li>
                        <li>Thuế <span>{{Cart::tax().' '.'vnđ'}}</span></li>
                        <li>Phí vận chuyển <span>Free</span></li>
                        <li>Thành tiền <span>{{Cart::total().' '.'vnđ'}}</span></li>
                    </ul>
                    {{-- 	<a class="btn btn-default update" href="">Update</a> --}}
                          <?php
                               $customer_id = Session::get('customer_id');
                               if($customer_id!=NULL){
                             ?>

                            <a class="btn btn-default check_out" href="{{URL::to('/checkout')}}">Thanh toán</a>
                            <?php
                        }else{
                             ?>

                             <a class="btn btn-default check_out" href="{{URL::to('/login-checkout')}}">Thanh toán</a>
                             <?php
                         }
                             ?>



                </div>
            </div>
        </div>
    </div>
</section><!--/#do_action-->

{{--  --}}
    <section id="cart_items" style="margin-top: 200px">
        <div class="container">
            <div class="row">
                <div id="columns_inner">
                    <div id="content-wrapper" class="left-column col-xs-12 col-sm-12 col-md-12">
                        <section id="main">

                                 <div class="cart-grid row">
                                <!-- Left Block: cart product informations & shpping -->
                                <div class="cart-grid-body col-xs-12 col-lg-8">
                                    <!-- cart products detailed -->
                                    <div class="card cart-container">
                                        <div class="card-block">
                                            <h1 class="h1">Địa chỉ nhận hàng</h1>
                                                <div class="form-one">
                                                    <form method="POST" style="text-align: left !important">
                                                    @csrf

                                                    Email: <input type="text" name="shipping_email" class="shipping_email" ><br>
                                                    Full Name: <input type="text" name="shipping_name" class="shipping_name" ><br>
                                                    Address: <input type="text" name="shipping_address" class="shipping_address" ><br>
                                                    Phone: <input type="text" name="shipping_phone" class="shipping_phone" ><br>
                                                    Note: <textarea name="shipping_notes" class="shipping_notes"  rows="5"></textarea>

                                                    @if(Session::get('fee'))
                                                        <input type="hidden" name="order_fee" class="order_fee" value="{{Session::get('fee')}}">
                                                    @else
                                                        <input type="hidden" name="order_fee" class="order_fee" value="10000">
                                                    @endif

                                                    @if(Session::get('coupon'))
                                                        @foreach(Session::get('coupon') as $key => $cou)
                                                            <input type="hidden" name="order_coupon" class="order_coupon" value="{{$cou['coupon_code']}}">
                                                        @endforeach
                                                    @else
                                                        <input type="hidden" name="order_coupon" class="order_coupon" value="no">
                                                    @endif
                                                    <div class="">
                                                        <div class="form-group">
                                                           <label for="exampleInputPassword1">Chọn hình thức thanh toán</label>
                                                             <select name="payment_select"  class="form-control input-sm m-bot15 payment_select">
                                                                   <input type="radio" id="paypal" value="0">Chuyển khoản qua PayPal
                                                                   <input type="radio" id="visa" value="1">Chuyển khoản qua Visa

                                                                   {{-- <input type="radio" id="r1" name="rr" />
                                                                   <label for="r1"><span></span>Radio Button 1</label>
                                                                   <p>
                                                                   <input type="radio" id="r2" name="rr" />
                                                                   <label for="r2"><span></span>Radio Button 2</label> --}}

                                                           </select>
                                                       </div>
                                                   </div>
                                                    <input type="button" value="Xác nhận đơn hàng" name="send_order" class="btn btn-primary btn-sm send_order">
                                                </form>

                                            </div>
                                        </div>
                                        <hr>

                                    </div>
                                    <div class="card cart-container">
                                        <div class="card-block">
                                            <h1 class="h1">Phương thức thanh toán</h1>
                                            <a href="">
                                                <div style="display:flex;text-align: left;">
                                                    <img src="images/paypal.png" width="90" alt="">
                                                        <p><span class="font-weight-bold">Thanh toán online thông qua paypal</span><br>
                                                        Khách hàng sử dụng phương thức chuyển khoản Paypal thanh toán đơn hàng</p>
                                                </div>
                                            </a>
                                        </div>
                                        <hr>

                                    </div>
                                    <a class="label" href="https://capricathemes.com/prestashop/PRS05/PRS050093/index.php">
                                    <i class="material-icons">chevron_left</i>Continue shopping
                                    </a>
                                    <!-- shipping informations -->
                                </div>

                                <!-- Right Block: cart subtotal & cart total -->
                                <div class="cart-grid-right col-xs-12 col-lg-4">
                                    <div class="card cart-summary">
                                        <div class="cart-detailed-totals">

                                            <form action="{{url('/update-cart')}}" method="POST">
                                                @csrf
                                                @php
                                                        $total = 0;
                                                @endphp
                                                @if(Session::get('cart')==true)


                                                    @foreach(Session::get('cart') as $key => $cart)
                                                        @php
                                                            $subtotal = ($cart['product_price']-$cart['product_sale_price'])*$cart['product_qty'];
                                                            $total+=$subtotal;
                                                        @endphp


                                                <div class="cart-item">
                                                    <div class="product-line-grid">
                                                        <div class="product-line-grid-left col-md-3 col-xs-4">
                                                            <span class="product-image media-middle">
                                                                <img src="{{asset('public/uploads/product/'.$cart['product_image'])}}" width="90" alt="{{$cart['product_name']}}" />
                                                            </span>
                                                        </div>
                                                      <div class="product-line-grid-body col-md-4 col-xs-8">
                                                            <div class="product-line-info">
                                                                    {{$cart['product_name']}}
                                                            </div>

                                                        </div>

                                                        <div class="product-line-grid-right product-line-actions col-md-5 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-xs-4 hidden-md-up"></div>
                                                                <div class="col-md-10 col-xs-6">
                                                                    <div class="row">
                                                                        <div class="col-md-6 col-xs-6 qty">
                                                                            <span class="product-price">
                                                                                <strong>
                                                                                    <p>{{$cart['product_qty']}}</p>

                                                                                </strong>
                                                                            </span>
                                                                        </div>
                                                                        <div class="col-md-6 col-xs-2 price">
                                                                            <span class="product-price">
                                                                            <strong>
                                                                                <p>{{'$'.number_format($subtotal,0,',','.')}}</p>

                                                                            </strong>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>

                                            @endforeach

                                                @else
                                                <tr>
                                                    <td colspan="5"><center>
                                                    @php
                                                    echo 'Làm ơn thêm sản phẩm vào giỏ hàng';
                                                    @endphp
                                                    </center></td>
                                                </tr>
                                                @endif
                                        </form>
                                            {{--  --}}
                                            <div class="card-block">

                                                <div class="cart-summary-line" id="cart-subtotal-shipping">
                                                    <span class="label">
                                                    Thanh toán:
                                                    </span>
                                                    <strong><span class="value">{{'$'.number_format($total,0,',','.')}}</span>  </strong>
                                                    <div><small class="value"></small></div>
                                                </div>
                                                <div class="cart-summary-line" id="cart-subtotal-shipping">
                                                    <span class="label">
                                                   Phí vận chuyển
                                                    </span>
                                                    <span class="value">$0</span>
                                                    <div><small class="value"></small></div>
                                                </div>

                                            </div>
                                            <hr>
                                            <div class="card-block">
                                                <div class="cart-summary-line cart-total">
                                                    <span class="label">Total: </span>
                                                    <strong><span class="value">{{'$'.number_format($total,0,',','.')}}</span>  </strong>
                                                </div>

                                            </div>
                                            <hr>
                                        </div>
                                        <div class="checkout cart-detailed-actions card-block">
                                            <div class="text-xs-center">
                                                <a href="https://capricathemes.com/prestashop/PRS05/PRS050093/index.php?controller=order" class="btn btn-primary">Checkout</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="block-reassurance" style="text-align:left !important">
                                        <ul>
                                            <li>
                                                <div class="block-reassurance-item">
                                                    <img src="images/icon-1.png" alt="Security policy (edit with Customer reassurance module)">
                                                    <span class="h6">Security policy (edit with Customer reassurance module)</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="block-reassurance-item">
                                                    <img src="images/icon-2.png" alt="Delivery policy (edit with Customer reassurance module)">
                                                    <span class="h6">Delivery policy (edit with Customer reassurance module)</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="block-reassurance-item">
                                                    <img src="images/icon3.png" alt="Return policy (edit with Customer reassurance module)">
                                                    <span class="h6">Return policy (edit with Customer reassurance module)</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection





{{-- <section id="wrapper" style="margin-top: 200px">
    <div class="container">
        <div class="row">
            <div id="columns_inner">

                <div id="content-wrapper" class="left-column col-xs-12 col-sm-12 col-md-12">
                    <section id="main">
                        <div class="cart-grid row">

                            <div class="cart-grid-body col-xs-12 col-lg-8">

                                <div class="card cart-container">
                                    <div class="card-block">
                                        <h1 class="h1">Shopping Cart</h1>
                                    </div>
                                    <hr>
                                    <div class="cart-overview js-cart" data-refresh-url="//capricathemes.com/prestashop/PRS05/PRS050093/index.php?controller=cart&amp;ajax=1&amp;action=refresh">
                                        <ul class="cart-items">
                                            <li class="cart-item">
                                                <div class="product-line-grid">

                                                    <div class="product-line-grid-left col-md-3 col-xs-4">
                                                        <span class="product-image media-middle">
                                                        <img src="https://capricathemes.com/prestashop/PRS05/PRS050093/img/p/1/1/1/111-cart_default.jpg" alt="et quasi architecto">
                                                        </span>
                                                    </div>

                                                    <div class="product-line-grid-body col-md-4 col-xs-8">
                                                        <div class="product-line-info">
                                                            <a class="label" href="https://capricathemes.com/prestashop/PRS05/PRS050093/index.php?id_product=10&amp;id_product_attribute=58&amp;rewrite=printed-summer-dress&amp;controller=product&amp;id_lang=1#/1-size-s/16-color-yellow" data-id_customization="0">et quasi architecto</a>
                                                        </div>
                                                        <div class="product-line-info">
                                                            <span class="value">$28.98</span>
                                                        </div>
                                                        <br>
                                                        <div class="product-line-info">
                                                            <span class="label">Size:</span>
                                                            <span class="value">S</span>
                                                        </div>
                                                        <div class="product-line-info">
                                                            <span class="label">Color:</span>
                                                            <span class="value">Yellow</span>
                                                        </div>
                                                    </div>

                                                    <div class="product-line-grid-right product-line-actions col-md-5 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-4 hidden-md-up"></div>
                                                            <div class="col-md-10 col-xs-6">
                                                                <div class="row">
                                                                    <div class="col-md-6 col-xs-6 qty">
                                                                        <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input class="js-cart-line-product-quantity form-control" data-down-url="https://capricathemes.com/prestashop/PRS05/PRS050093/index.php?controller=cart&amp;update=1&amp;id_product=10&amp;id_product_attribute=58&amp;token=d0079bf62de7300595f3731cc8e67631&amp;op=down" data-up-url="https://capricathemes.com/prestashop/PRS05/PRS050093/index.php?controller=cart&amp;update=1&amp;id_product=10&amp;id_product_attribute=58&amp;token=d0079bf62de7300595f3731cc8e67631&amp;op=up" data-update-url="https://capricathemes.com/prestashop/PRS05/PRS050093/index.php?controller=cart&amp;update=1&amp;id_product=10&amp;id_product_attribute=58&amp;token=d0079bf62de7300595f3731cc8e67631" data-product-id="10" type="text" value="2" name="product-quantity-spin" min="1" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><button class="btn btn-touchspin js-touchspin js-increase-product-quantity bootstrap-touchspin-up" type="button"><i class="material-icons touchspin-up"></i></button><button class="btn btn-touchspin js-touchspin js-decrease-product-quantity bootstrap-touchspin-down" type="button"><i class="material-icons touchspin-down"></i></button></span></div>
                                                                    </div>
                                                                    <div class="col-md-6 col-xs-2 price">
                                                                        <span class="product-price">
                                                                        <strong>
                                                                        $57.95
                                                                        </strong>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 col-xs-2 text-xs-right">
                                                                <div class="cart-line-product-actions">
                                                                    <a class="remove-from-cart" rel="nofollow" href="https://capricathemes.com/prestashop/PRS05/PRS050093/index.php?controller=cart&amp;delete=1&amp;id_product=10&amp;id_product_attribute=58&amp;token=d0079bf62de7300595f3731cc8e67631" data-link-action="delete-from-cart" data-id-product="10" data-id-product-attribute="58" data-id-customization="">
                                                                    <i class="material-icons pull-xs-left">delete</i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <a class="label" href="https://capricathemes.com/prestashop/PRS05/PRS050093/index.php">
                                <i class="material-icons">chevron_left</i>Continue shopping
                                </a>

                            </div>

                            <div class="cart-grid-right col-xs-12 col-lg-4">
                                <div class="card cart-summary">
                                    <div class="cart-detailed-totals">
                                        <div class="card-block">
                                            <div class="cart-summary-line" id="cart-subtotal-products">
                                                <span class="label js-subtotal">
                                                2 items
                                                </span>
                                                <span class="value">$57.95</span>
                                            </div>
                                            <div class="cart-summary-line" id="cart-subtotal-shipping">
                                                <span class="label">
                                                Shipping
                                                </span>
                                                <span class="value">$7.00</span>
                                                <div><small class="value"></small></div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="card-block">
                                            <div class="cart-summary-line cart-total">
                                                <span class="label">Total (tax excl.)</span>
                                                <span class="value">$64.95</span>
                                            </div>
                                            <div class="cart-summary-line">
                                                <small class="label">Taxes</small>
                                                <small class="value">$0.00</small>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="checkout cart-detailed-actions card-block">
                                        <div class="text-xs-center">
                                            <a href="https://capricathemes.com/prestashop/PRS05/PRS050093/index.php?controller=order" class="btn btn-primary">Checkout</a>
                                        </div>
                                    </div>
                                </div>
                                <div id="block-reassurance">
                                    <ul>
                                        <li>
                                            <div class="block-reassurance-item">
                                                <img src="https://capricathemes.com/prestashop/PRS05/PRS050093/modules/blockreassurance/img/ic_verified_user_black_36dp_1x.png" alt="Security policy (edit with Customer reassurance module)">
                                                <span class="h6">Security policy (edit with Customer reassurance module)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="block-reassurance-item">
                                                <img src="https://capricathemes.com/prestashop/PRS05/PRS050093/modules/blockreassurance/img/ic_local_shipping_black_36dp_1x.png" alt="Delivery policy (edit with Customer reassurance module)">
                                                <span class="h6">Delivery policy (edit with Customer reassurance module)</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="block-reassurance-item">
                                                <img src="https://capricathemes.com/prestashop/PRS05/PRS050093/modules/blockreassurance/img/ic_swap_horiz_black_36dp_1x.png" alt="Return policy (edit with Customer reassurance module)">
                                                <span class="h6">Return policy (edit with Customer reassurance module)</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section> --}}

