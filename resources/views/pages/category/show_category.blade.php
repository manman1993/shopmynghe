@extends('layout')
@section('content')
{{-- <div class="features_items"><!--features_items-->
    <div class="fb-share-button" data-href="http://localhost/tutorial_youtube/shopbanhanglaravel" data-layout="button_count" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{$url_canonical}}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
        <div class="fb-like" data-href="{{$url_canonical}}" data-width="" data-layout="button_count" data-action="like" data-size="small" data-share="false"></div>
                        @foreach($category_name as $key => $name)
                        <h2 class="title text-center">{{$name->category_name}}</h2>
                        @endforeach
                        @foreach($category_by_id as $key => $product)
                        <a href="{{URL::to('/chi-tiet/'.$product->product_slug)}}">
                        <div class="col-sm-4">
                             <div class="product-image-wrapper">

                                <div class="single-products">
                                        <div class="productinfo text-center">
                                            <form>
                                                @csrf
                                            <input type="hidden" value="{{$product->product_id}}" class="cart_product_id_{{$product->product_id}}">
                                            <input type="hidden" value="{{$product->product_name}}" class="cart_product_name_{{$product->product_id}}">
                                            <input type="hidden" value="{{$product->product_image}}" class="cart_product_image_{{$product->product_id}}">
                                            <input type="hidden" value="{{$product->product_quantity}}" class="cart_product_quantity_{{$product->product_id}}">
                                            <input type="hidden" value="{{$product->product_price}}" class="cart_product_price_{{$product->product_id}}">
                                            <input type="hidden" value="1" class="cart_product_qty_{{$product->product_id}}">

                                            <a href="{{URL::to('/chi-tiet/'.$product->product_slug)}}">
                                                <img src="{{URL::to('public/uploads/product/'.$product->product_image)}}" alt="" />
                                                <h2>{{number_format($product->product_price,0,',','.').' '.'VNĐ'}}</h2>
                                                <p>{{$product->product_name}}</p>


                                             </a>
                                            <input type="button" value="Thêm giỏ hàng" class="btn btn-default add-to-cart" data-id_product="{{$product->product_id}}" name="add-to-cart">
                                            </form>

                                        </div>

                                </div>

                                <div class="choose">
                                    <ul class="nav nav-pills nav-justified">
                                        <li><a href="#"><i class="fa fa-plus-square"></i>Yêu thích</a></li>
                                        <li><a href="#"><i class="fa fa-plus-square"></i>So sánh</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        </a>
                        @endforeach
</div><!--features_items-->
                   <ul class="pagination pagination-sm m-t-none m-b-none">
                       {!!$category_by_id->links()!!}
                    </ul> --}}




        <div class="row">
            @foreach($category_name as $key => $name)
            <h2 class="h1 products-section-title">
                <span>{{$name->category_name}}</span>
             </h2>
            @endforeach

            <div class="products hb-animate-element right-to-left">
               <div class="product_special">
                  <ul id="special-carousel" class="ct-carousel product_list">
                    @foreach($category_by_id as $key => $product)
                    <a href="{{URL::to('/chi-tiet/'.$product->product_slug)}}">
                     <li class="item">
                        <div class="product-miniature js-product-miniature" data-id-product="5" data-id-product-attribute="19" itemscope >
                           <div class="thumbnail-container">
                            <form>
                                @csrf
                            <input type="hidden" value="{{$product->product_id}}" class="cart_product_id_{{$product->product_id}}">
                            <input type="hidden" value="{{$product->product_name}}" class="cart_product_name_{{$product->product_id}}">
                            <input type="hidden" value="{{$product->product_quantity}}" class="cart_product_quantity_{{$product->product_id}}">
                            <input type="hidden" value="{{$product->product_image}}" class="cart_product_image_{{$product->product_id}}">
                            <input type="hidden" value="{{$product->product_price}}" class="cart_product_price_{{$product->product_id}}">
                            <input type="hidden" value="1" class="cart_product_qty_{{$product->product_id}}">
                            <a href="{{URL::to('/chi-tiet/'.$product->product_slug)}}">
                                <img src="{{URL::to('public/uploads/product/'.$product->product_image)}}" alt="" />
                             </a>
                            <input type="button" value="Thêm giỏ hàng" class="btn btn-default add-to-cart" data-id_product="{{$product->product_id}}" name="add-to-cart">
                            </form>

                           </div>
                           <div class="product-description">

                              <h1 class="h3 product-title" itemprop="name">
                                <a href="{{URL::to('/chi-tiet/'.$product->product_slug)}}">
                                    <p>{{$product->product_name}}</p>
                                 </a>
                                </h1>

                            <?php
                                if($product->product_sale_price !=0){
                            ?>
                                <div class="product-price-and-shipping">
                                    <span style="text-decoration-line: line-through" itemprop="price " class="regular-price">{{'$'.(number_format($product->product_price,0,',','.'))}}</span>
                                   <span class="discount-percentage">{{'-$'.(number_format($product->product_sale_price,0,',','.'))}}</span>
                                   <span class="price">{{'$'.(number_format($product->product_price - $product->product_sale_price,0,',','.'))}}</span>
                                 </div>
                             <?php
                         }else{
                              ?>
                              <div class="product-price-and-shipping">
                                <span itemprop="price " class="price">{{'$'.(number_format($product->product_price,0,',','.'))}}</span>
                             </div>
                              <?php
                          }
                              ?>
                           </div>
                        </div>
                     </li>
                    </a>
                     @endforeach
                  </ul>

               </div>
            </div>
        </div>
        <ul class="pagination pagination-sm m-t-none m-b-none">
            {!!$category_by_id->links()!!}
           </ul>
@endsection
