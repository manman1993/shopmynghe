@extends('layout')
@section('content')


    <section id="cart_items">
        <div class="container">
            <div class="row">
                <div id="columns_inner">
                    <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
                        <div class="block-categories block">
                            <h4 class="block_title hidden-md-down">
                            <a href="{{'/'}}">Home</a>
                            </h4>
                            <h4 class="block_title hidden-lg-up" data-target="#block_categories_toggle" data-toggle="collapse">
                                <a href="{{'/'}}">Home</a>
                                <span class="pull-xs-right">
                                <span class="navbar-toggler collapse-icons">
                                <i class="material-icons add"></i>
                                <i class="material-icons remove"></i>
                                </span>
                                </span>
                            </h4>
                            <div id="block_categories_toggle" class="block_content collapse">
                                <ul class="category-top-menu">
                                    @foreach($category as $key => $cate)
                                        <li class="category" id="category-3">
                                        <a  class="dropdown-item"
                                            href="{{URL::to('/danh-muc/'.$cate->slug_category_product)}}" data-depth="0">
                                            {{$cate->category_name}}
                                        </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                        <div class="sidebar-specials block">
                            <h4 class="block_title hidden-md-down">Sale Products</h4>
                            <h4 class="block_title hidden-lg-up" data-target="#block_specials_toggle" data-toggle="collapse">Special Products
                                <span class="pull-xs-right">
                                <span class="navbar-toggler collapse-icons">
                                <i class="material-icons add"></i>
                                <i class="material-icons remove"></i>
                                </span>
                                </span>
                            </h4>

                            <div class="block_content collapse" id="block_specials_toggle">
                                <div class="products clearfix">
                                    @foreach($sale_product as $key => $cart)
                                        <div class="product-item">
                                            <div class="left-part">

                                                <a href="{{URL::to('/chi-tiet/'.$cart->product_slug)}}">
                                                    <span class="product-image media-middle">
                                                        <img src="{{URL::to('public/uploads/product/'.$cart->product_image)}}" width="90" alt="" />
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="right-part">
                                                <div class="product-description">

                                                <h1 class="h3 product-title" itemprop="name"><a href=""></a></h1>
                                                    <div class="product-price-and-shipping">
                                                    <p>{{$cart->product_name}}</p>
                                                    <span style="text-decoration-line: line-through" itemprop="price " class="regular-price">{{'$'.(number_format($cart->product_price,0,',','.'))}}</span>
                                                    <span class="discount-percentage">{{'-$'.(number_format($cart->product_sale_price,0,',','.'))}}</span>
                                                    <span class="price">{{'$'.(number_format($cart->product_price - $cart->product_sale_price,0,',','.'))}}</span>
                                                    </div>
                                                </div>
                                                <div class="product-actions">
                                                    <form>
                                                        @csrf
                                                    <input type="hidden" value="{{$cart->product_id}}" class="cart_product_id_{{$cart->product_id}}">
                                                    <input type="hidden" value="{{$cart->product_name}}" class="cart_product_name_{{$cart->product_id}}">
                                                    <input type="hidden" value="{{$cart->product_quantity}}" class="cart_product_quantity_{{$cart->product_id}}">
                                                    <input type="hidden" value="{{$cart->product_image}}" class="cart_product_image_{{$cart->product_id}}">
                                                    <input type="hidden" value="{{$cart->product_price}}" class="cart_product_price_{{$cart->product_id}}">
                                                    <input type="hidden" value="{{$cart->product_sale_price}}" class="cart_product_sale_price_{{$cart->product_id}}">
                                                    <input type="hidden" value="1" class="cart_product_qty_{{$cart->product_id}}">

                                                    <input type="button" value="Thêm giỏ hàng" class="btn btn-default add-to-cart" data-id_product="{{$cart->product_id}}" name="add-to-cart">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="clearfix">
                                    @foreach($brand as $key => $brand1)



                                    <a href="{{URL::to('/thuong-hieu/'.$brand1->brand_slug)}}" class="allproducts">All sale products</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    </div>

                    <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
                        <section id="main">
                            <div class="cart-grid row">
                                <!-- Left Block: cart product informations & shpping -->
                                <div class="cart-grid-body col-xs-12 col-lg-8">
                                    <!-- cart products detailed -->
                                    <div class="card cart-container">
                                        <div class="card-block">
                                            <h1 class="h1">Shopping Cart</h1>
                                        </div>
                                        <hr>
                                        @if(session()->has('message'))
                                        <div class="alert alert-success">
                                            {!! session()->get('message') !!}
                                        </div>
                                        @elseif(session()->has('error'))
                                         <div class="alert alert-danger">
                                            {!! session()->get('error') !!}
                                        </div>
                                        @endif

                                        <div class="cart-overview js-cart" >
                                            <ul class="cart-items">
                                                <form action="{{url('/update-cart')}}" method="POST">
                                                        @csrf
                                                        @php
                                                                $total = 0;
                                                        @endphp
                                                       @if(Session::get('cart')==true)

                                                         {{-- @if(Session::get('cart')==true) --}}
                                                            @foreach(Session::get('cart') as $key => $cart)
                                                                @php
                                                                    $subtotal = ($cart['product_price']-$cart['product_sale_price'])*$cart['product_qty'];
                                                                    $total+=$subtotal;
                                                                @endphp


                                                        <li class="cart-item">
                                                            <div class="product-line-grid">
                                                                <div class="product-line-grid-left col-md-3 col-xs-4">
                                                                    <span class="product-image media-middle">
                                                                        <img src="{{asset('public/uploads/product/'.$cart['product_image'])}}" width="90" alt="{{$cart['product_name']}}" />
                                                                    </span>
                                                                </div>
                                                              <div class="product-line-grid-body col-md-4 col-xs-8">
                                                                    <div class="product-line-info">
                                                                            {{$cart['product_name']}}
                                                                    </div>
                                                                    <div class="product-line-info">
                                                                        <span class="value">{{'$'.number_format($cart['product_price']-$cart['product_sale_price'],0,',','.')}}</span>

                                                                    </div>
                                                                    <br>

                                                                </div>

                                                                <div class="product-line-grid-right product-line-actions col-md-5 col-xs-12">
                                                                    <div class="row">
                                                                        <div class="col-xs-4 hidden-md-up"></div>
                                                                        <div class="col-md-10 col-xs-6">
                                                                            <div class="row">
                                                                                <div class="col-md-6 col-xs-6 qty">
                                                                                    <div class="input-group bootstrap-touchspin">


                                                                                        <div class="cart_quantity_button">
                                                                                            <input style="color: black" class="cart_quantity js-cart-line-product-quantity form-control" type="number" min="1" name="cart_qty[{{$cart['session_id']}}]" value="{{$cart['product_qty']}}"  >
                                                                                        </div>

                                                                                        <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;">
                                                                                        </span>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-xs-2 price">
                                                                                    <span class="product-price">
                                                                                    <strong>
                                                                                        <p>{{'$'.number_format($subtotal,0,',','.')}}</p>

                                                                                    </strong>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2 col-xs-2 text-xs-right">
                                                                            <div class="cart-line-product-actions">

                                                                                <a class="remove-from-cart cart_quantity_delete" rel="nofollow" href="{{url('/del-product/'.$cart['session_id'])}}" ><i class="material-icons pull-xs-left">delete</i></a>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </li>

                                                    @endforeach

                                                        <tr>
                                                            <td><input type="submit" value="Cập nhật giỏ hàng" name="update_qty" class="check_out btn btn-default btn-sm btn btn-primary"></td>
                                                            <td colspan="2">

                                                        </td>
                                                        </tr>
                                                        @else
                                                        <tr>
                                                            <td colspan="5"><center>
                                                            @php
                                                            echo 'Làm ơn thêm sản phẩm vào giỏ hàng';
                                                            @endphp
                                                            </center></td>
                                                        </tr>
                                                        @endif
                                                </form>
                                            </ul>
                                        </div>
                                    </div>

                                <a class="label" href="{{'/'}}">
                                    <i class="material-icons">chevron_left</i>Continue shopping
                                    </a>
                                    <!-- shipping informations -->
                                </div>
                                <!-- Right Block: cart subtotal & cart total -->
                                <div class="cart-grid-right col-xs-12 col-lg-4">
                                    <div class="card cart-summary">

                                            <div class="cart-detailed-totals">
                                                <div class="card-block">
                                                    <div class="cart-summary-line">
                                                        <small class="label">Taxes</small>
                                                        <small class="value">$0.00</small>
                                                    </div>
                                                    <div class="cart-summary-line cart-total">
                                                        <span style="font-size: 15px" class="label">Total: </span>
                                                        <span style="font-size: 15px;font-weight:bold" class="value">{{'$'.number_format($total,0,',','.')}}</span>
                                                    </div>

                                                </div>
                                                <hr>
                                            </div>

                                        <div class="checkout cart-detailed-actions card-block">
                                            <div class="text-xs-center">
                                                {{-- <a href="https://capricathemes.com/prestashop/PRS05/PRS050093/index.php?controller=order" class="btn btn-primary">Checkout</a> --}}
                                                @if(Session::get('customer_id'))
                                                <a class="btn btn-primary" href="{{url('/checkout')}}">Checkout</a>
                                                @else
                                                <a class="btn btn-primary" href="{{url('/dang-nhap')}}">Checkout</a>
                                              @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div id="block-reassurance" style="text-align:left !important">
                                        <ul>
                                            <li>
                                                <div class="block-reassurance-item">
                                                    <img src="images/icon-1.png" alt="Security policy (edit with Customer reassurance module)">
                                                    <span class="h6">Security policy (edit with Customer reassurance module)</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="block-reassurance-item">
                                                    <img src="images/icon-2.png" alt="Delivery policy (edit with Customer reassurance module)">
                                                    <span class="h6">Delivery policy (edit with Customer reassurance module)</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="block-reassurance-item">
                                                    <img src="images/icon3.png" alt="Return policy (edit with Customer reassurance module)">
                                                    <span class="h6">Return policy (edit with Customer reassurance module)</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection



