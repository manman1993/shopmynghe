 @extends('layout')
@section('content')
@foreach($product_details as $key => $value)
<section id="wrapper">
    <div class="container">
       <div class="row">

          <div id="columns_inner">
             <div id="content-wrapper">


                <section id="main" itemscope="" >

                   <div class="row">
                      <div class="col-md-6 product_left">
                         <section class="page-content" id="content">
                            <div class="product-leftside">
                               <ul class="product-flags">
                                  <li class="product-flag on-sale">On sale!</li>
                                  <li class="product-flag new">New</li>
                               </ul>
                               <div class="images-container">
                                  <div class="product-cover">
                                     <img class="js-qv-product-cover" src="{{URL::to('/public/uploads/product/'.$value->product_image)}}" alt="" title="" style="width:100%;" itemprop="image">
                                     <div class="layer hidden-sm-down" data-toggle="modal" data-target="#product-modal">
                                        <i class="material-icons zoom-in"></i>
                                     </div>
                                  </div>

                               </div>
                               <div class="scroll-box-arrows">
                                  <i class="material-icons left"></i>
                                  <i class="material-icons right"></i>
                               </div>
                            </div>
                         </section>

                      </div>

                      <div class="col-md-6 product_right">
                      <h1 class="h1 productpage_title" itemprop="name">{{$value->product_name}}</h1>
                         <div class="product-prices">
                            <div class="product-price h5 ">
                               <link itemprop="availability">
                               <meta itemprop="priceCurrency" content="USD">
                               <div class="product-price-and-shipping">



                            </div>
                            </div>
                            <div class="tax-shipping-delivery-label">
                            </div>
                         </div>

                         <div class="product-information">


                            <p>ID: {{$value->product_id}}</p>
                            <h2 style="text-decoration-line:line-through" itemprop="price" class="price">Our Price: {{'$'.(number_format($value->product_price,0,',','.'))}}</h2>
                            <h2  itemprop="price" class=" button-cart">SAVINGS: {{'$'.(number_format($value->product_sale_price,0,',','.'))}}</h2>

                            <h2 itemprop="price" class="price">Sale Price: {{'$'.(number_format($value->product_price-$value->product_sale_price,0,',','.'))}}</h2>


                            <form action="{{URL::to('/save-cart')}}" method="POST">
                                @csrf
                                <input type="hidden" value="{{$value->product_id}}" class="cart_product_id_{{$value->product_id}}">

                                        <input type="hidden" value="{{$value->product_name}}" class="cart_product_name_{{$value->product_id}}">

                                        <input type="hidden" value="{{$value->product_image}}" class="cart_product_image_{{$value->product_id}}">

                                        <input type="hidden" value="{{$value->product_quantity}}" class="cart_product_quantity_{{$value->product_id}}">

                                        <input type="hidden" value="{{$value->product_price}}" class="cart_product_price_{{$value->product_id}}">
                                        <input type="hidden" value="{{$value->product_sale_price}}" class="cart_product_sale_price_{{$value->product_id}}">

                            <span>

                                <label>Quantity:</label>
                                <input name="qty" type="number" min="1" class="cart_product_qty_{{$value->product_id}}"  value="1" />
                                <input name="productid_hidden" type="hidden"  value="{{$value->product_id}}" />
                            </span>
                            <input type="button" value=" + ADD TO CART" class="btn btn-primary btn-sm add-to-cart" data-id_product="{{$value->product_id}}" name="add-to-cart">
                            </form>

                            <div class="product-quantities">
                                <span>{{$value->product_quantity}}</span>
                                <label class="label">In stock</label>
                             </div>


                        </div>


                      </div>
                   </div>


                </section>
                <div class="category-tab shop-details-tab">
                    <div class="col-sm-12">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#details" data-toggle="tab">Mô tả</a></li>
                            <li><a href="#companyprofile" data-toggle="tab">Chi tiết sản phẩm</a></li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="details" >
                            <p>{!!$value->product_desc!!}</p>
                        </div>
                        <div class="tab-pane fade" id="companyprofile" >
                            <p>{!!$value->product_content!!}</p>
                        </div>
                    </div>
                </div>
             </div>
          </div>

       </div>
    </div>
 </section>
 @endforeach







                    <div class="row">
                        <h2 class="h1 products-section-title">
                           <span>You Might Also Like</span>
                        </h2>

                        <div class="products hb-animate-element right-to-left">
                           <div class="product_special">
                              <ul id="special-carousel" class="ct-carousel product_list">
                                @foreach($relate as $key => $lienquan)

                                 <li class="item">
                                    <div class="product-miniature js-product-miniature" data-id-product="5" data-id-product-attribute="19" itemscope >
                                       <div class="thumbnail-container">
                                        <form>
                                            @csrf
                                        <input type="hidden" value="{{$lienquan->product_id}}" class="cart_product_id_{{$lienquan->product_id}}">
                                        <input type="hidden" value="{{$lienquan->product_name}}" class="cart_product_name_{{$lienquan->product_id}}">
                                        <input type="hidden" value="{{$lienquan->product_quantity}}" class="cart_product_quantity_{{$lienquan->product_id}}">
                                        <input type="hidden" value="{{$lienquan->product_image}}" class="cart_product_image_{{$lienquan->product_id}}">
                                        <input type="hidden" value="{{$lienquan->product_price}}" class="cart_product_price_{{$lienquan->product_id}}">
                                        <input type="hidden" value="1" class="cart_product_qty_{{$lienquan->product_id}}">
                                        <a href="{{URL::to('/chi-tiet/'.$lienquan->product_slug)}}">
                                            <img src="{{URL::to('public/uploads/product/'.$lienquan->product_image)}}" alt="" />
                                         </a>
                                        <input type="button" value="Thêm giỏ hàng" class="btn btn-default add-to-cart" data-id_product="{{$lienquan->product_id}}" name="add-to-cart">
                                        </form>

                                       </div>
                                       <div class="product-description">
                                          <h1 class="h3 product-title" itemprop="name">
                                            <a href="{{URL::to('/chi-tiet/'.$lienquan->product_slug)}}">
                                                <p>{{$lienquan->product_name}}</p>
                                             </a>
                                            </h1>
                                          <div class="product-price-and-shipping">
                                             <span itemprop="price" class="price">{{'$'.(number_format($lienquan->product_price,0,',','.'))}}</span>
                                            <span class="discount-percentage">{{'$'.(number_format($lienquan->product_sale_price,0,',','.'))}}</span>
                                            <span class="regular-price">{{'$'.(number_format($lienquan->product_price - $lienquan->product_sale_price,0,',','.'))}}</span>
                                          </div>
                                          <div class="product-actions">
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                                 @endforeach
                              </ul>

                           </div>
                        </div>
                    </div>
					  <ul class="pagination pagination-sm m-t-none m-b-none">
                       {!!$relate->links()!!}
                      </ul>



@endsection

