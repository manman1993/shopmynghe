@extends('layout')
@section('content')
{{-- <div class="features_items"><!--features_items-->
                        <h2 class="title text-center">Kết quả tìm kiếm</h2>
                       @foreach($search_product as $key => $product)
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                             <a href="{{URL::to('/chi-tiet-san-pham/'.$product->product_slug)}}">
                                <div class="single-products">
                                        <div class="productinfo text-center">
                                            <img src="{{URL::to('public/uploads/product/'.$product->product_image)}}" alt="" />
                                            <h2>{{number_format($product->product_price).' '.'VNĐ'}}</h2>
                                            <p>{{$product->product_name}}</p>
                                            <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Thêm giỏ hàng</a>
                                        </div>

                                </div>
                            </a>
                                <div class="choose">
                                    <ul class="nav nav-pills nav-justified">
                                        <li><a href="#"><i class="fa fa-plus-square"></i>Yêu thích</a></li>
                                        <li><a href="#"><i class="fa fa-plus-square"></i>So sánh</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div><!--features_items-->
        <!--/recommended_items--> --}}


        <div class="row">
            <h2 class="h1 products-section-title">
               <span>Kết quả tìm kiếm</span>
            </h2>

            <div class="products hb-animate-element right-to-left">
               <div class="product_special">
                  <ul id="special-carousel" class="ct-carousel product_list">
                    @foreach($search_product as $key => $product)

                     <li class="item">
                        <div class="product-miniature js-product-miniature" data-id-product="5" data-id-product-attribute="19" itemscope >
                           <div class="thumbnail-container">
                            <form>
                                @csrf
                            <input type="hidden" value="{{$product->product_id}}" class="cart_product_id_{{$product->product_id}}">
                            <input type="hidden" value="{{$product->product_name}}" class="cart_product_name_{{$product->product_id}}">
                            <input type="hidden" value="{{$product->product_quantity}}" class="cart_product_quantity_{{$product->product_id}}">
                            <input type="hidden" value="{{$product->product_image}}" class="cart_product_image_{{$product->product_id}}">
                            <input type="hidden" value="{{$product->product_price}}" class="cart_product_price_{{$product->product_id}}">
                            <input type="hidden" value="1" class="cart_product_qty_{{$product->product_id}}">
                            <a href="{{URL::to('/chi-tiet/'.$product->product_slug)}}">
                                <img src="{{URL::to('public/uploads/product/'.$product->product_image)}}" alt="" />
                             </a>
                            <input type="button" value="Thêm giỏ hàng" class="btn btn-default add-to-cart" data-id_product="{{$product->product_id}}" name="add-to-cart">
                            </form>

                           </div>
                           <div class="product-description">
                              <h1 class="h3 product-title" itemprop="name">
                                <a href="{{URL::to('/chi-tiet/'.$product->product_slug)}}">
                                    <p>{{$product->product_name}}</p>
                                 </a>
                                </h1>
                              <div class="product-price-and-shipping">
                                 <span itemprop="price" class="price">{{'$'.(number_format($product->product_price,0,',','.'))}}</span>
                                <span class="discount-percentage">{{'$'.(number_format($product->product_sale_price,0,',','.'))}}</span>
                                <span class="regular-price">{{'$'.(number_format($product->product_price - $product->product_sale_price,0,',','.'))}}</span>
                              </div>
                              <div class="product-actions">
                              </div>
                           </div>
                        </div>
                     </li>
                     @endforeach
                  </ul>

               </div>
            </div>
        </div>
          {{-- <ul class="pagination pagination-sm m-t-none m-b-none">
           {!!$relate->links()!!}
          </ul> --}}
@endsection
