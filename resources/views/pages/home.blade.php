@extends('layout')
@section('content')
                <div class="row">
                        <h2 class="h1 products-section-title">
                           <span>Special products</span>
                        </h2>

                        <div class="products hb-animate-element right-to-left">
                           <div class="product_special">
                              <ul id="special-carousel" class="ct-carousel product_list">
                                @foreach($all_product as $key => $product)

                                 <li class="item">
                                    <div class="product-miniature js-product-miniature" data-id-product="5" data-id-product-attribute="19" itemscope >
                                       <div class="thumbnail-container">
                                        <form>
                                            @csrf
                                        <input type="hidden" value="{{$product->product_id}}" class="cart_product_id_{{$product->product_id}}">
                                        <input type="hidden" value="{{$product->product_name}}" class="cart_product_name_{{$product->product_id}}">
                                        <input type="hidden" value="{{$product->product_quantity}}" class="cart_product_quantity_{{$product->product_id}}">
                                        <input type="hidden" value="{{$product->product_image}}" class="cart_product_image_{{$product->product_id}}">
                                        <input type="hidden" value="{{$product->product_price}}" class="cart_product_price_{{$product->product_id}}">
                                        <input type="hidden" value="{{$product->product_sale_price}}" class="cart_product_sale_price_{{$product->product_id}}">
                                        <input type="hidden" value="1" class="cart_product_qty_{{$product->product_id}}">
                                        <a href="{{URL::to('/chi-tiet/'.$product->product_slug)}}">
                                            <img src="{{URL::to('public/uploads/product/'.$product->product_image)}}" alt="" />
                                         </a>
                                        <input type="button" value="Thêm giỏ hàng" class="btn btn-default add-to-cart" data-id_product="{{$product->product_id}}" name="add-to-cart">
                                        </form>

                                       </div>
                                       <div class="product-description">
                                          <h1 class="h3 product-title" itemprop="name">
                                            <a href="{{URL::to('/chi-tiet/'.$product->product_slug)}}">
                                                <p>{{$product->product_name}}</p>
                                             </a>
                                            </h1>

                                        <?php
                                            if($product->product_sale_price !=0){
                                        ?>
                                            <div class="product-price-and-shipping">
                                                <span style="text-decoration-line: line-through" itemprop="price " class="regular-price">{{'$'.(number_format($product->product_price,0,',','.'))}}</span>
                                               <span class="discount-percentage">{{'-$'.(number_format($product->product_sale_price,0,',','.'))}}</span>
                                               <span class="price">{{'$'.(number_format($product->product_price - $product->product_sale_price,0,',','.'))}}</span>
                                             </div>
                                         <?php
                                     }else{
                                          ?>
                                          <div class="product-price-and-shipping">
                                            <span itemprop="price " class="price">{{'$'.(number_format($product->product_price,0,',','.'))}}</span>
                                         </div>
                                          <?php
                                      }
                                          ?>
                                          <div class="product-actions">
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                                 @endforeach
                              </ul>

                           </div>
                        </div>
                    </div>
                    <ul class="pagination pagination-sm m-t-none m-b-none">
                        {!!$all_product->links()!!}
                       </ul>
@endsection

